package miage.florian.banquespring.repositories;

import miage.florian.banquespring.models.Compte;
import org.springframework.data.repository.CrudRepository;

public interface CompteRepo extends CrudRepository<Compte, Long> {
}
