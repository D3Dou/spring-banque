package miage.florian.banquespring.repositories;

import miage.florian.banquespring.models.Operation;
import org.springframework.data.repository.CrudRepository;

public interface OperationRepo extends CrudRepository<Operation, Long> {
}
