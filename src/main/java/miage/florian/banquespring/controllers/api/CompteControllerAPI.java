package miage.florian.banquespring.controllers.api;

import miage.florian.banquespring.models.Compte;
import miage.florian.banquespring.models.Operation;
import miage.florian.banquespring.models.exceptions.CompteIntrouvableException;
import miage.florian.banquespring.models.exceptions.ProcedureInvalideException;
import miage.florian.banquespring.services.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/API/Compte")
public class CompteControllerAPI {

    @Autowired
    private ICompteService compteService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public @ResponseStatus ResponseEntity<Compte> ouvrirUnNouveauCompte(){
        return ResponseEntity.ok(compteService.ouvrirUnNouveauCompte());
    }

    @RequestMapping(value = "/Operation/Debiter", method = RequestMethod.POST)
    public @ResponseStatus ResponseEntity<Operation> debiterUnCompte(@RequestBody Operation operation) {
        try{
            operation = compteService.debiterUnCompte(operation);
            return ResponseEntity.ok(operation);
        }catch (CompteIntrouvableException e){
            return (ResponseEntity) ResponseEntity.notFound();
        }catch (ProcedureInvalideException e){
            return (ResponseEntity) ResponseEntity.badRequest();
        }
    }

    @RequestMapping(value = "/Operation/Crediter", method = RequestMethod.POST)
    public @ResponseStatus ResponseEntity crediterUnCompte(@RequestBody Operation operation) {
        try{
            operation = compteService.crediterUnCompte(operation);
            return ResponseEntity.ok(operation);
        }catch (CompteIntrouvableException e){
            return (ResponseEntity) ResponseEntity.notFound();
        }catch (ProcedureInvalideException e){
            return (ResponseEntity) ResponseEntity.badRequest();
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public @ResponseStatus ResponseEntity<Compte> voirCompte(@RequestParam(value="compteID") int compteID) {
        try{
            Compte compte = compteService.voirCompte(compteID);
            return ResponseEntity.ok(compte);
        }catch (CompteIntrouvableException e){
            return (ResponseEntity<Compte>) ResponseEntity.notFound();
        }
    }

    @RequestMapping(value = "/Fermer", method = RequestMethod.PATCH)
    public @ResponseStatus ResponseEntity fermerUnCompte(@RequestParam(value="compteID") int compteID){
        try{
            compteService.fermerUnCompte(compteID);
            return ResponseEntity.ok(HttpStatus.ACCEPTED);
        }catch (CompteIntrouvableException e){
            return ResponseEntity.ok(HttpStatus.NOT_FOUND);
        }catch (ProcedureInvalideException e){
            return ResponseEntity.ok(HttpStatus.BAD_REQUEST);
        }
    }
}
