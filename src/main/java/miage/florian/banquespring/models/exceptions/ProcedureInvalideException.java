package miage.florian.banquespring.models.exceptions;

public class ProcedureInvalideException extends Exception{

    public ProcedureInvalideException(String raison) {
        super(raison);
    }
}
