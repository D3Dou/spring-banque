package miage.florian.banquespring.models.exceptions;

public class CompteIntrouvableException extends Exception{

    public CompteIntrouvableException() {
        super("Le compte est introuvable");
    }
}
