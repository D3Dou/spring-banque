package miage.florian.banquespring.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Operation {

    public enum TypeOperation{
        Debit,
        Credit
    }

    @Id
    @GeneratedValue
    private Long id;
    private TypeOperation type;
    private Double montant;
    private String intitule;
    private Date date;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "operations", nullable = true)
    private Compte compte;

    public Operation() {
    }

    public Operation(TypeOperation type, Double montant, String intitule, Compte compte) {
        this.type = type;
        this.montant = montant;
        this.intitule = intitule;
        this.date = new Date();
        this.compte = compte;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TypeOperation getType() {
        return type;
    }

    public void setType(TypeOperation type) {
        this.type = type;
    }

    public Compte getCompte() {
        return compte;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
