package miage.florian.banquespring.models;

import miage.florian.banquespring.models.exceptions.ProcedureInvalideException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Compte {

    @Id
    @GeneratedValue
    private Long id;
    private Double montant;
    private Boolean etat;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Operation> operations;

    public Compte() {
        this.montant = 0.0;
        this.etat = true;
        this.operations = new ArrayList<>();
    }

    public void debiter(Operation operation) throws ProcedureInvalideException{
        if(!etat){
            throw new ProcedureInvalideException("Le compte est fermé");
        }
        this.operations.add(operation);
        this.montant-= operation.getMontant();
    }

    public void crediter(Operation operation) throws ProcedureInvalideException {
        if(!etat){
            throw new ProcedureInvalideException("Le compte est fermé");
        }
        this.operations.add(operation);
        this.montant+= operation.getMontant();
    }

    public void fermer() throws ProcedureInvalideException{
        if(!etat){
            throw new ProcedureInvalideException("Le compte est fermé");
        }
        etat = false;
    }

    public Long getId() {
        return id;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Boolean isEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }
}
