package miage.florian.banquespring.services;

import miage.florian.banquespring.models.Compte;
import miage.florian.banquespring.models.Operation;
import miage.florian.banquespring.models.exceptions.CompteIntrouvableException;
import miage.florian.banquespring.models.exceptions.ProcedureInvalideException;

public interface ICompteService {

    Compte ouvrirUnNouveauCompte();

    Operation debiterUnCompte(Operation demandeOperationVM) throws CompteIntrouvableException, ProcedureInvalideException;

    Operation crediterUnCompte(Operation demandeOperationVM) throws CompteIntrouvableException, ProcedureInvalideException;

    Compte voirCompte(long compteID) throws CompteIntrouvableException;

    void fermerUnCompte(long compteID) throws CompteIntrouvableException, ProcedureInvalideException;

}
