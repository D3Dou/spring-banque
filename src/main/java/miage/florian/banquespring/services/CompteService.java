package miage.florian.banquespring.services;

import miage.florian.banquespring.models.Compte;
import miage.florian.banquespring.models.Operation;
import miage.florian.banquespring.models.exceptions.CompteIntrouvableException;
import miage.florian.banquespring.models.exceptions.ProcedureInvalideException;
import miage.florian.banquespring.repositories.CompteRepo;
import miage.florian.banquespring.repositories.OperationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompteService implements ICompteService {

    @Autowired
    private CompteRepo compteRepo;

    @Autowired
    private OperationRepo operationRepo;

    @Override
    public Compte ouvrirUnNouveauCompte() {
        Compte compte  = new Compte();
        compteRepo.save(compte);
        return compte;
    }

    @Override
    public Operation debiterUnCompte(Operation operation) throws CompteIntrouvableException, ProcedureInvalideException {
        Optional<Compte> compte = compteRepo.findById(operation.getCompte().getId());
        if(compte.isEmpty()){
            throw new CompteIntrouvableException();
        }
        Operation operationE = new Operation(Operation.TypeOperation.Debit,operation.getMontant(), operation.getIntitule(), compte.get());
        compte.get().debiter(operationE);
        operationRepo.save(operationE);
        return operationE;
    }

    @Override
    public Operation crediterUnCompte(Operation operation) throws CompteIntrouvableException, ProcedureInvalideException {
        Optional<Compte> compte = compteRepo.findById(operation.getCompte().getId());
        if(compte.isEmpty()){
            throw new CompteIntrouvableException();
        }
        Operation operationE = new Operation(Operation.TypeOperation.Credit,operation.getMontant(), operation.getIntitule(), compte.get());
        compte.get().crediter(operationE);
        operationRepo.save(operationE);
        return operationE;
    }

    @Override
    public Compte voirCompte(long compteID) throws CompteIntrouvableException{
        Optional<Compte> compte = compteRepo.findById(compteID);
        if(compte.isEmpty()){
            throw new CompteIntrouvableException();
        }
        return compte.get();
    }

    @Override
    public void fermerUnCompte(long compteID) throws CompteIntrouvableException, ProcedureInvalideException {
        Optional<Compte> compte = compteRepo.findById(compteID);
        if(compte.isEmpty()){
            throw new CompteIntrouvableException();
        }
        compte.get().fermer();
    }
}
